function CreateUser() {
    var data = {
        fname: document.getElementById("fname").value,
        lname: document.getElementById("lname").value,
        email: document.getElementById("email").value,
        password: document.getElementById("password").value,
        pw2 : document.getElementById("pw2").value,
        profileP : "images/user.png"
    }
    if (data.fname == "" || data.email == "" || data.password == ""){
        alert("Enter the information completely")
        return 
    }
    if (data.password != data.pw2){
        alert("Password is not matching")
        return 
    }
    fetch("/signup",{
        method: "POST",
        body: JSON.stringify(data),
        headers:{"content-type":"application/json; charset-utf 8"}
    }).then(response => {
       if(response.status == 201){
        // var pic = GetProfilePicture()
        // console.log(pic)
            window.location.href = "index.html"
       }else if (response.status == 406){
            alert("The email is already registered with us, try logging in")
       }else{
            throw new Error(response.statusText)
       }
    }).catch(e =>{
        alert(e)
    })
}




