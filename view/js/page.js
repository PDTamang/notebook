var em = localStorage.getItem("em")
// alert(em)
var bid = localStorage.getItem("bid")
window.onload = function(){
  fetch("/book/"+bid)
  .then(resp => resp.text())
  .then(data => showData(data))
  .catch(e =>{
    alert(e)
  })
}

function showData(data){
  const content = JSON.parse(data)
  document.getElementById("myTextarea").innerHTML = content.Page
}
const fontSelect = document.getElementById('fontSelect');
const textarea = document.getElementById('myTextarea');
const fontSizeButton = document.getElementById('fontSizeButton');

fontSizeButton.addEventListener('click', () => {
  const newFontSize = prompt('Enter a font size (in pixels):');
  if (newFontSize) {
    textarea.style.fontSize = `${newFontSize}px`;
  }
});

fontSelect.addEventListener('change', () => {
  textarea.style.fontFamily = fontSelect.value;
});


function savePage(){
  var Page = {
    Page :document.getElementById("myTextarea").value
  }
  fetch("/books/"+bid,{
    method:"PATCH",
    body:JSON.stringify(Page),
    headers:{"content-type":"application/json"}
  })
  .then(resp =>{
    if (resp.ok){
      window.location.href = "shelf.html"
    }else{
      throw new Error(resp.statusText)
    }
  })
  .catch(e =>{
    alert(e)
  })
}