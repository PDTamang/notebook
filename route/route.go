package route

import (
	"NoteBook/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func Initialize() {
	router := mux.NewRouter()

	router.HandleFunc("/signup", controller.SignupHandler).Methods("POST")
	router.HandleFunc("/login", controller.LoginHandler).Methods("POST")
	router.HandleFunc("/logout", controller.LogoutHandler).Methods("GET")
	router.HandleFunc("/user/{email}", controller.UpdateUserHandler).Methods("PUT")

	router.HandleFunc("/profile/{email}", controller.ProfileHandler).Methods("GET")
	router.HandleFunc("/book", controller.CreateBookHandler).Methods("POST")
	router.HandleFunc("/book/{bid}", controller.GetBookHandler).Methods("GET")
	router.HandleFunc("/book/{bid}", controller.UpdateTitleHandler).Methods("PUT")
	router.HandleFunc("/books/{bid}", controller.UpdatePageHandler).Methods("PATCH")
	router.HandleFunc("/book/{bid}", controller.DeleteBookHandler).Methods("DELETE")
	router.HandleFunc("/books/{em}", controller.GetAllBooksHandeler).Methods("GET")
	// router.HandleFunc("/books/{bid}", controller.GetContentPage).Methods("GET")

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)
	http.ListenAndServe(":8081", router)
}
