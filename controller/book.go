package controller

import (
	"NoteBook/httpResp"
	"NoteBook/model"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func CreateBookHandler(w http.ResponseWriter, r *http.Request) {
	// if !VerifyCookie(w, r) {
	// 	return
	// }
	fmt.Println("hii")
	var bok model.Book
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bok)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		fmt.Println("kskf", err)
		return
	}
	fmt.Println("kdkkfkkk", bok.BookName)
	dbErr := bok.CreateBook()
	if dbErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		fmt.Println("skdfsa", dbErr)
		return
	} else {
		httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "book is added successfully"})
		fmt.Println("success")

	}
}

func GetBookID(bid string) int64 {
	bbid, _ := strconv.ParseInt(bid, 10, 64)
	return bbid
}
func GetBookHandler(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	bid := mux.Vars(r)["bid"]
	bbid := GetBookID(bid)
	bok := model.Book{BookID: bbid}
	getErr := bok.ReadBook()

	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "book not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, bok)
	}
}

func UpdateTitleHandler(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	old_bid := mux.Vars(r)["bid"]
	old_bid_int := GetBookID(old_bid)
	var bok model.Book
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bok)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	updateTitleErr := bok.UpdateTitle(old_bid_int)
	if updateTitleErr != nil {
		switch updateTitleErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "book not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateTitleErr.Error())
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, bok)
	}
}

func UpdatePageHandler(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	bid := mux.Vars(r)["bid"]
	fmt.Println("hi", bid)
	int_bid := GetBookID(bid)
	var bok model.Book
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bok)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		fmt.Println("1", err)
		return
	}

	updatePageErr := bok.UpdatePage(int_bid)
	if updatePageErr != nil {
		switch updatePageErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "book not found")
			fmt.Println("2", updatePageErr)
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updatePageErr.Error())
			fmt.Println("3", updatePageErr)
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, bok)
		fmt.Println("success")
	}
}

func DeleteBookHandler(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	bid := mux.Vars(r)["bid"]
	int_bid := GetBookID(bid)
	bok := model.Book{BookID: int_bid}
	err := bok.DeleteBook()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}
func GetAllBooksHandeler(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	fmt.Println("hellooo brother")
	em := mux.Vars(r)["em"]
	allBooks, err := model.GetAllBooks(em)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println("1", err)
		return
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, allBooks)
		fmt.Println("2", err)
	}
}
