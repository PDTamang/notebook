CREATE TABLE books (
    BookID SERIAL PRIMARY Key,
    BookName varchar(15) NOT NULL,
    Page TEXT
)