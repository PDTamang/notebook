create database project;

create table users (
    FirstName varchar(45) not null ,
    LastName varchar(45) default null,
    Email varchar(45) primary key,
    Password text not null
)

ALTER TABLE users ALTER COLUMN Password TYPE text;
