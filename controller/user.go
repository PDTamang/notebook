package controller

import (
	"NoteBook/httpResp"
	"NoteBook/model"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func SignupHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("reaching ")
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "couldn't decode the request")
		fmt.Println("error in decoding the request", err)
		return
	}
	fmt.Println("profile picture", user.ProfilePicture)
	var hash []byte
	hash, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("bcrypt err:", err)
		return
	}
	fmt.Println("hash:", hash)
	fmt.Println("string(hash)", string(hash))
	addErr := user.CreateUser(hash)

	if addErr != nil {
		if e, ok := addErr.(*pq.Error); ok {
			if e.Code == "23505" {
				fmt.Print("duplicate key error")
				httpResp.RespondWithError(w, http.StatusNotAcceptable, "duplicate key error")
				return
			}
		} else {
			fmt.Println("error in inserting the data")
			httpResp.RespondWithError(w, http.StatusBadRequest, "error in inserting the data")
			return
		}
	}
	// if addErr != nil {
	// 	fmt.Print("error in inserting the data",addErr)
	// 	httpResp.RespondWithError(w,http.StatusBadRequest,"error in adding the data")
	// 	return
	// }
	httpResp.RespondWithJson(w, http.StatusCreated, "added successfully")
	fmt.Println("successful")
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User
	var err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println("error in decoding the request")
		return
	}

	dbHash, checkErr := user.Check()
	if checkErr != nil {
		switch checkErr {
		case sql.ErrNoRows:
			fmt.Print("invalid login")
			httpResp.RespondWithError(w, http.StatusUnauthorized, "Invalid login, try again")

		default:
			fmt.Print("error in getting the data to compare", checkErr)
			httpResp.RespondWithError(w, http.StatusBadRequest, "error with the database")

		}
		return
	}
	pwdErr := bcrypt.CompareHashAndPassword([]byte(dbHash), []byte(user.Password))
	if pwdErr != nil {
		fmt.Println("invalid with password")
		httpResp.RespondWithError(w, http.StatusUnauthorized, "Invalid login, try again")
		return
	}
	cookie := http.Cookie{
		Name:    "user-cookie",
		Value:   "#@Furpa77",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	fmt.Println("login successful")
	httpResp.RespondWithJson(w, http.StatusAccepted, map[string]string{"message": "successful"})
}
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("kkkkkkkk")
	http.SetCookie(w, &http.Cookie{
		Name:    "user-cookie",
		Expires: time.Now(),
	})
	fmt.Println("logout successful")
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "logout successful"})
}
func ProfileHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("reaching here")
	email := mux.Vars(r)["email"]
	var user model.User
	getErr := user.GetProfile(email)
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "error in getting from database")
		fmt.Println("couldn't get from database")
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, user)
	fmt.Println("result", user)
}

func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var user model.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	dbErr := user.UpdateUser(email)
	if dbErr != nil {
		switch dbErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "User not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, dbErr.Error())
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, user)
	}
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("user-cookie")
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not set")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		}
		return false
	}
	if cookie.Value != "#@Furpa77" {
		httpResp.RespondWithError(w, http.StatusSeeOther, "invalid cookie")
		return false
	}
	return true
}
