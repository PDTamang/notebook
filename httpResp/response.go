package httpResp

import (
	"encoding/json"
	"net/http"
)

func RespondWithError(w http.ResponseWriter, code int, message string) {
	RespondWithJson(w, code, map[string]string{"error":message} )
}

func RespondWithJson(w http.ResponseWriter, code int, payload interface{}){
	data, _ := json.Marshal(payload)
	w.Header().Set("Content-type","application/json")
	w.WriteHeader(code)
	w.Write(data)
}