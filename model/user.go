package model

import (
	postgres "NoteBook/datastore"
	"fmt"
)

type User struct {
	FirstName      string `json:"fname"`
	LastName       string `json:"lname"`
	Email          string `json:"email"`
	Password       string `json:"password"`
	ProfilePicture string `json:"profileP"`
}

func (u *User) CreateUser(a []byte) error {
	fmt.Println("model")
	const queryCreateUser = "INSERT INTO you (FirstName, LastName,Email,Password,profileP) VALUES($1,$2,$3,$4,$5);"
	_, err := postgres.Db.Exec(queryCreateUser, u.FirstName, u.LastName, u.Email, a, u.ProfilePicture)
	fmt.Println("err", err)
	return err
}

func (u *User) Check() ([]byte, error) {
	var dbHash []byte
	const queryCheck = "SELECT * FROM you WHERE Email = $1;"
	err := postgres.Db.QueryRow(queryCheck, u.Email).Scan(&u.FirstName, &u.LastName, &u.Email, &dbHash, &u.ProfilePicture)
	return dbHash, err
}

func (u *User) GetProfile(email string) error {
	const queryGetProfile = "Select * from you where email = $1;"
	err := postgres.Db.QueryRow(queryGetProfile, email).Scan(&u.FirstName, &u.LastName, &u.Email, &u.Password, &u.ProfilePicture)
	return err
}

func (u *User) UpdateUser(email string) error {
	const queryUdateUser = "UPDATE you set FirstName = $1, LastName = $2, profileP = $3 WHERE Email = $4 RETURNING Email"
	err := postgres.Db.QueryRow(queryUdateUser, u.FirstName, u.LastName, u.ProfilePicture, email).Scan(&u.Email)
	fmt.Println(err, "here")
	return err
}
