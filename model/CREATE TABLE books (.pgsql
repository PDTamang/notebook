CREATE TABLE books (
    BookID   text PRIMARY KEY,
	BookName VARCHAR(39) NOT NULL,
	Page     TEXT,
	UserEmail    TEXT NOT NULL,
    FOREIGN KEY (UserEmail) REFERENCES users(Email) 
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

DROP TABLE books;