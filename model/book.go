package model

import postgres "NoteBook/datastore"

type Book struct {
	BookID   int64
	BookName string
	Page     string
	Email    string
}

const (
	queryInsertBook  = "INSERT INTO books(BookName,Page,Email) VALUES($1,$2,$3)"
	queryReadBook    = "SELECT * FROM books WHERE BookID = $1"
	queryUpdateTitle = "UPDATE books set BookName = $1 WHERE BookID = $2 RETURNING BookID,BookName,Page"
	queryUpdatePage  = "UPDATE books set Page = $1 WHERE BookID = $2 RETURNING BookID,BookName,Page;"
	queryDeletedBook = "DELETE FROM books WHERE BookID = $1 RETURNING BookID"
	queryGetAllBooks = "SELECT * FROM books where Email = $1;"
)

func (b *Book) CreateBook() error {
	_, err := postgres.Db.Exec(queryInsertBook, b.BookName, b.Page, b.Email)
	return err
}
func (b *Book) ReadBook() error {
	err := postgres.Db.QueryRow(queryReadBook, b.BookID).Scan(&b.BookID, &b.BookName, &b.Page, &b.Email)
	return err
}
func (b *Book) UpdateTitle(old_bid int64) error {
	err := postgres.Db.QueryRow(queryUpdateTitle, b.BookName, old_bid).Scan(&b.BookID, &b.BookName, &b.Page)
	return err
}

func (b *Book) UpdatePage(old_bid int64) error {
	err := postgres.Db.QueryRow(queryUpdatePage, b.Page, old_bid).Scan(&b.BookID, &b.BookName, &b.Page)
	return err
}
func (b *Book) DeleteBook() error {
	if err := postgres.Db.QueryRow(queryDeletedBook, b.BookID).Scan(&b.BookID); err != nil {
		return err
	}
	return nil
}
func GetAllBooks(em string) ([]Book, error) {
	rows, err := postgres.Db.Query(queryGetAllBooks, em)
	if err != nil {
		return nil, err
	}
	books := []Book{}
	for rows.Next() {
		var bok Book
		dbErr := rows.Scan(&bok.BookID, &bok.BookName, &bok.Page, &bok.Email)
		if dbErr != nil {
			return nil, dbErr
		}
		books = append(books, bok)
	}
	rows.Close()
	return books, nil
}
